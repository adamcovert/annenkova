$(document).ready(() => {

  var promoSwiper = new Swiper('.s-promo__slider', {
    speed: 400,
    navigation: {
      prevEl: '.s-promo__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-promo__slider-nav .s-slider-nav__btn--next'
    }
  });

  var teamSwiper = new Swiper('.s-team__slider', {
    speed: 400,
    spaceBetween: 20,
    navigation: {
      prevEl: '.s-team__slider-nav .s-slider-nav__btn--prev',
      nextEl: '.s-team__slider-nav .s-slider-nav__btn--next'
    },
    breakpoints: {
      320: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      480: {
        slidesPerView: 3,
        spaceBetween: 30
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 20
      }
    }
  });

  var promoSwiper = new Swiper('.testimonials-alt__slider', {
    speed: 400,
    pagination: {
      el: '.testimonials-alt__slider-pag',
      type: 'fraction',
    },
    navigation: {
      prevEl: '.testimonials-alt__slider-nav-btn--prev',
      nextEl: '.testimonials-alt__slider-nav-btn--next'
    }
  });



  $('.s-page-header__burger').on('click', function () {
    $(this).toggleClass('s-page-header__burger--is-active');
    $('.s-page-header__nav').toggleClass('s-page-header__nav--is-active');
  });

  $('.s-page-header__search-toggler').on('click', function () {
    $('.s-page-header__search-form').toggleClass('s-page-header__search-form--is-active');
  });
});